from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    """
    Таблица профиля пользователя
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    first_name = models.CharField(max_length=100, verbose_name='Имя')
    last_name = models.CharField(max_length=100, verbose_name='Фамилия')
    patronymic = models.CharField(max_length=100, verbose_name='Отчество')
    phone = models.CharField(max_length=12, null=False, blank=False, unique=True, verbose_name='Телефон')
    birth_date = models.DateField(null=False, blank=False, verbose_name='Дата рождения')

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'

    def __str__(self):
        return self.first_name+' '+self.last_name


class PCAssembly(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название')
    article = models.CharField(max_length=50, verbose_name='Артикул')
    description = models.TextField(verbose_name='Описание')
    image = models.ImageField(verbose_name='Изображние', null=True, blank=True)

    class Meta:
        verbose_name = 'Комплектующие'
        verbose_name_plural = 'Комплектующие'

    def __str__(self):
        return self.name


class Tariff(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название')
    price_hour = models.PositiveIntegerField(verbose_name='Цена за час')
    description = models.TextField(verbose_name='Описание')
    pc_assembly = models.ManyToManyField(PCAssembly, verbose_name='Комплектующие')

    class Meta:
        verbose_name = 'Тариф'
        verbose_name_plural = 'Тариф'

    def __str__(self):
        return self.name


class Reservation(models.Model):
    """
    Таблица бронирования
    """
    reserve_from = models.DateTimeField(null=False, blank=False, verbose_name='От')
    reserve_to = models.DateTimeField(null=False, blank=False, verbose_name='До')
    people_amount = models.PositiveIntegerField(verbose_name='Количество людей')
    user = models.ForeignKey(Profile, on_delete=models.CASCADE, verbose_name='Пользователь')
    tariff = models.ForeignKey(Tariff, on_delete=models.CASCADE, verbose_name='Тариф', default=None, null=True)

    class Meta:
        verbose_name = 'Бронь'
        verbose_name_plural = 'Брони'


class Game(models.Model):
    """
    таблица игр
    """
    name = models.CharField(max_length=100, verbose_name='Название')
    description = models.TextField(verbose_name='Описание')
    release_date = models.DateField(verbose_name='Дата релиза')
    developer = models.CharField(max_length=100, verbose_name='Разработчик')
    photo = models.ImageField(verbose_name='Фото')
    category = models.ForeignKey('category', on_delete=models.CASCADE, null=True, blank=True, verbose_name='Категория')

    class Meta:
        verbose_name = 'Игра'
        verbose_name_plural = 'Игры'

    def __str__(self):
        return self.name


class Category(models.Model):
    """
    Таблица категорий
    """
    name = models.CharField(max_length=100, verbose_name='название')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name

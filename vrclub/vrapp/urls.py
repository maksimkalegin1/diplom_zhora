from django.urls import path

from .views import *

urlpatterns = [
    path('', index),
    path('games/', games),
    path('devices/', devices),
    path('login/', login_view),
    path('signup/', register_view),
    path('logout/', logout_view),
    path('game_detail/<int:game_pk>/', game_detail),
    path('reservation/', reservation)
]
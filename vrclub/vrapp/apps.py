from django.apps import AppConfig


class VrappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'vrapp'
    verbose_name = 'digital club'
    verbose_name_plural = 'digital club'
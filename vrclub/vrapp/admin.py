from django.contrib import admin
from .models import *


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'first_name', 'last_name', 'patronymic', 'phone']
    search_fields = ['user', 'first_name', 'last_name', 'phone']
    list_display_links = ['user', 'first_name', 'last_name', 'patronymic', 'phone']

@admin.register(Reservation)
class ReservationAdmin(admin.ModelAdmin):
    list_display = ['reserve_from', 'reserve_to', 'people_amount', 'user']
    search_fields = ['reserve_from', 'reserve_to', 'people_amount', 'user']


@admin.register(Tariff)
class TariffAdmin(admin.ModelAdmin):
    list_display = ['name', 'price_hour']


@admin.register(PCAssembly)
class PCAssemblyAdmin(admin.ModelAdmin):
    list_display = ['name', 'article']


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    list_display = ['name', 'category', 'release_date', 'developer']
    search_fields = ['name', 'release_date', 'developer']
    list_filter = ['category']

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass

